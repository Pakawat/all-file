import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late GoogleMapController mapController;

  final LatLng _center = const LatLng(7.5187188416153115, 99.57879806295585);

  Future<void> _onMapCreated(GoogleMapController controller) async {
    mapController = controller;
    String value = await DefaultAssetBundle.of(context)
        .loadString('assets/map_style.json');
    mapController.setMapStyle(value);
  }

  Set<Marker> _createMarker() {
    return {
      Marker(
        markerId: MarkerId("marker_1"),
        position: _center,
        infoWindow: InfoWindow(
            title: 'PSU Trang',
            snippet: "มหาวิทยาลัยสงขลานครินทร์ วิทยาเขตตรัง"),
        icon: BitmapDescriptor.defaultMarker,
      ),
      Marker(
        markerId: MarkerId("marker_2"),
        position: LatLng(7.512611281453871, 99.61534341732121),
        infoWindow: InfoWindow(title: 'Trang Airport', snippet: "สนามบินตรัง"),
        rotation: 90,
        //icon: customIcon,
        icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueCyan),
      ),
      Marker(
        markerId: MarkerId("marker_3"),
        position: LatLng( 7.553668, 99.563263),
        infoWindow: InfoWindow(title: 'Home', snippet: "บ้าน"),
        rotation: 90,
        //icon: customIcon,
        icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueCyan),
      ),
    };
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Maps Sample App'),
          backgroundColor: Colors.green[700],
        ),
        body: GoogleMap(
          myLocationEnabled: true,
          mapToolbarEnabled: true,
          //mapType: MapType.hybrid,
          onMapCreated: _onMapCreated,
          initialCameraPosition: CameraPosition(
            target: _center,
            zoom: 15.0,
          ),
          markers: _createMarker(),
        ),
      ),
    );
  }
}
