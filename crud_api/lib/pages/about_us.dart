import 'package:flutter/material.dart';

class AboutUs extends StatelessWidget {
  const AboutUs({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: const Text('About Us'),
        centerTitle: true,
      ),
      body: Container(
          margin: const EdgeInsets.all(42),
          child: const Column(
            children: [
              Text('DEVELOPER', style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold),),
              Divider(thickness: 3),
              ListTile(
                leading: CircleAvatar(backgroundImage: AssetImage('assets/pond.png')),
                title: Text("Kirtsakon Tanalatakun", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),),
                subtitle: Text("Student ID: 6450110001"),
              ),
              ListTile(
                leading: CircleAvatar(backgroundImage: AssetImage('assets/pooh.png')),
                title: Text("Pakawat Promeam", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),),
                subtitle: Text("Student ID: 6450110009"),
              ),
              ListTile(
                leading: CircleAvatar(backgroundImage: AssetImage('assets/sor.png')),
                title: Text("Weeraphat Songsrijan", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),),
                subtitle: Text("Student ID: 6450110012"),
              ),
              ListTile(
                leading: CircleAvatar(backgroundImage: AssetImage('assets/note.jpg')),
                title: Text("Sittisak Choolek", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),),
                subtitle: Text("Student ID: 6450110013"),
              ),
              Divider(thickness: 3),
              SizedBox(height: 18),
              Text(
                  'Information and Digital Technology Management',
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.w400),
                  textAlign: TextAlign.center
              ),
              Text(
                  'Prince of Songkla University, Trang Campus',
                  style: TextStyle(fontSize: 12, fontWeight: FontWeight.normal),
                  textAlign: TextAlign.center
              ),
            ],
          )
      ),
    );
  }
}