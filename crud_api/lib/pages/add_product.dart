import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class AddProduct extends StatefulWidget {
  const AddProduct({super.key});

  @override
  State<AddProduct> createState() => _AddProductState();
}

class _AddProductState extends State<AddProduct> {
  TextEditingController nameController = TextEditingController();
  TextEditingController priceController = TextEditingController();
  TextEditingController descController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  // Http post request to create new data
  Future _createProduct() async {
    return await http.post(
      Uri.parse("http://10.0.2.2/android/insert_product.php"),
      body: {
        "name": nameController.text,
        "price": priceController.text,
        "description": descController.text
      },
    );
  }

  void _onConfirm(context) async {
    await _createProduct();
    // Remove all existing routes until the main.dart, then rebuild Home.
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Crate Product'),
        centerTitle: true,
      ),
      body: Container(
        height: double.infinity,
        padding: const EdgeInsets.all(20),
        child: Column(
          children: <Widget>[
            Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    buildTextFormField(ctrl: nameController, label: 'Name', errorText: 'Enter product name'),
                    buildTextFormField(ctrl: priceController, label: 'Price', errorText: 'Enter product price'),
                    buildTextFormField(ctrl: descController, label: 'Description', errorText: 'Enter product description'),
                  ],
                ),
            ),

            // TextField(
            //   controller: nameController,
            //   decoration: const InputDecoration(
            //     labelText: "Product Name:",
            //   ),
            // ),
            // TextField(
            //   controller: priceController,
            //   decoration: const InputDecoration(
            //     labelText: "Price:",
            //   ),
            // ),
            // TextField(
            //   controller: descController,
            //   decoration: const InputDecoration(
            //     labelText: "Description:",
            //   ),
            // ),
          ],
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            ElevatedButton.icon(
              label: const Text('Save'),
              icon: const Icon(Icons.save),
              onPressed: () {
                // Validate returns true if the form is valid, or false otherwise.
                if (_formKey.currentState!.validate()) {
                  // If the form is valid, display a snack bar. In the real world,
                  // you'd often call a server or save the information in a database.
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(content: Text('Add product successful...')),
                  );
                }
                _onConfirm(context);
              },
            ),
            ElevatedButton.icon(
              label: const Text('Cancel'),
              icon: const Icon(Icons.cancel_rounded),
              onPressed: () => Navigator.of(context)
                  .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false),
            )
          ],
        ),
      ),
    );
  }

  TextFormField buildTextFormField({required String label, required final ctrl, required String errorText}) {
    return TextFormField(
      controller: ctrl,
      decoration: InputDecoration(
        labelText: '$label:',
      ),
      // The validator receives the text that the user has entered.
      validator: (value) {
        if (value == null || value.isEmpty) {
          return errorText;
        }
        return null;
      },
    );
  }
}
