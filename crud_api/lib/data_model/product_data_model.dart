// To parse this JSON data, do
// final productDataModel = productDataModelFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

List<ProductDataModel> productDataModelFromJson(String str) => List<ProductDataModel>.from(json.decode(str).map((x) => ProductDataModel.fromJson(x)));

String productDataModelToJson(List<ProductDataModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ProductDataModel {
  final String pid;
  final String name;
  final String price;
  final String description;

  ProductDataModel({
    required this.pid,
    required this.name,
    required this.price,
    required this.description,
  });

  factory ProductDataModel.fromJson(Map<String, dynamic> json) => ProductDataModel(
    pid: json["pid"],
    name: json["name"],
    price: json["price"],
    description: json["description"],
  );

  Map<String, dynamic> toJson() => {
    "pid": pid,
    "name": name,
    "price": price,
    "description": description,
  };
}
