class DiaryModel {

  int? id;
  String title;
  String date;
  String description;
  String image;

  DiaryModel({
    this.id,
    required this.title,
    required this.date,
    required this.description,
    required this.image,
  });

  factory DiaryModel.fromMap(Map<String, dynamic> json) =>
      DiaryModel(
        id: json['id'],
        title: json['title'],
        date: json['date'],
        description: json['description'],
        image: json['image'],
      );

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'title': title,
      'date': date,
      'description': description,
      'image': image,
    };
  }
}