import 'dart:io';

import 'package:flutter/material.dart';
import 'package:sqflite_midterm_project/database/sql_helper.dart';
import 'package:sqflite_midterm_project/model/diary_model.dart';

class ShowDiary extends StatelessWidget {
  const ShowDiary({required this.id, super.key});
  final id;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Diary Detail'),
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        centerTitle: true,
      ),
      body: Container(
        color: Colors.greenAccent[100],
        margin: EdgeInsets.zero,
        child: FutureBuilder<List<DiaryModel>>(
          future: SQLHelper.useDatabase.getDiary(id),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return const CircularProgressIndicator(
                backgroundColor: Colors.tealAccent,
                valueColor: AlwaysStoppedAnimation<Color>(Colors.amberAccent),
                strokeWidth: 5,
              );
            }
            return snapshot.data!.isEmpty
                ? const Center(child: Text('No Diary in List.'))
                : ListView(
              children: snapshot.data!.map((diary) {
                return Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Image.file(
                        diary.image as File,
                        fit: BoxFit.fitHeight,
                      ),
                      Container(
                        padding: const EdgeInsets.all(10),
                        child: Column(
                          children: [
                            Text(
                              diary.title,
                              style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.black),
                            ),
                            Text(
                              diary.date,
                              style: const TextStyle(fontSize: 12, fontWeight: FontWeight.normal, color: Colors.white12),
                            ),
                            const Divider(color: Colors.tealAccent),
                            Text(
                              diary.description,
                              style: const TextStyle(fontSize: 15, fontWeight: FontWeight.normal, color: Colors.black),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                );
              }).toList(),
            );
          },
        ),
      ),
    );
  }
}
