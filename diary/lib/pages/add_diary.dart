import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:sqflite_midterm_project/database/sql_helper.dart';
import 'package:sqflite_midterm_project/model/diary_model.dart';

class AddDiary extends StatefulWidget {
  const AddDiary({super.key});

  @override
  State<AddDiary> createState() => _AddDiaryState();
}

class _AddDiaryState extends State<AddDiary> {

  var titleController = TextEditingController();
  var dateController = TextEditingController();
  var descriptionController = TextEditingController();
  var _image;
  var imagePicker;

  @override
  void initState() {
    super.initState();
    imagePicker = ImagePicker();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Add Diary'),
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        centerTitle: true,
      ),
      body: Container(
        color: Colors.greenAccent[100],
        margin: EdgeInsets.zero,
        child: ListView(
          children: [
            const Padding(
              padding: EdgeInsets.all(8),
              child: Text(
                'How about today?',
                style: TextStyle(color: Colors.black, fontSize: 25, fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8),
              child: GestureDetector(
                onTap: () async {
                  final image = await imagePicker.pickImage(
                    source: ImageSource.gallery,
                    imageQuality: 50,
                  );
                  setState(() {
                    _image = File(image.path);
                  });
                },
                child: Container(
                  constraints: const BoxConstraints(maxWidth: 350.0, minHeight: 220),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(7.0),
                      color: Colors.black54),
                  child: _image != null
                      ? Image.file(
                    _image,
                    fit: BoxFit.fitHeight,
                  )
                      : Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(7.0),
                        color: Colors.black54),
                    constraints: const BoxConstraints(maxWidth: 350.0),
                    child: const Icon(
                      Icons.camera,
                      size: 75,
                      color: Colors.lime,
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                padding: const EdgeInsets.all(12),
                constraints: const BoxConstraints(minWidth: 350.0, minHeight: 300),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 0, bottom: 5),
                      child: headerRow(text: 'TITLE', hint: 'Topic for your diary.', ctrl: titleController),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 5, bottom: 20),
                      child: headerRow(text: 'DATE', hint: 'e.g. 01/12/2023', ctrl: dateController),
                    ),
                    const Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Icon(
                          Icons.add_box_rounded,
                          color: Colors.black,
                        ),
                        Text(
                          'DESCRIPTION',
                          style: TextStyle(fontSize: 17.5, fontWeight: FontWeight.bold, color: Colors.black),
                        ),
                      ],
                    ),
                    const Divider(color: Colors.teal),
                    TextField(
                      controller: descriptionController,
                      maxLines: null,
                      style: const TextStyle(color: Colors.black),
                      decoration: const InputDecoration(
                          border: InputBorder.none,
                          filled: true,
                          fillColor: Colors.white
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  buildElevatedButton('Cancel', false),
                  buildElevatedButton('Add', true),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Row headerRow({required String text, required final ctrl, required String hint}) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      children: [
        SizedBox(
          width: 90,
          child: Text(
            '$text: ',
            style: const TextStyle(
                color: Colors.black,
                fontSize: 20,
                fontWeight: FontWeight.bold),
          ),
        ),
        SizedBox(
          width: 260,
          child: TextField(
            controller: ctrl,
            style: const TextStyle(color: Colors.black),
            decoration: InputDecoration(
                hintText: hint,
                hintStyle: const TextStyle(
                  color: Colors.black45,
                  fontWeight: FontWeight.normal,
                ),
                filled: true,
                fillColor: Colors.white
            ),
          ),
        ),
      ],
    );
  }

  ElevatedButton buildElevatedButton(String text, bool active) {
    return ElevatedButton(
      onPressed: () async {
        if (text == 'Add') {
          await SQLHelper.useDatabase.add(
            DiaryModel(
                title: titleController.text,
                date: dateController.text,
                description: descriptionController.text,
                image: _image.path
            ),
          );
          if (!context.mounted) return;
          Navigator.pop(context);
        } else {
          Navigator.pop(context);
        }
      },
      style: active == true
          ? ElevatedButton.styleFrom(
        foregroundColor: Colors.white,
        fixedSize: const Size(120, 50),
        padding: const EdgeInsets.symmetric(horizontal: 2.5),
        textStyle: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        elevation: 5,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
        ),
      )
          : ElevatedButton.styleFrom(
        foregroundColor: Colors.green,
        fixedSize: const Size(120, 50),
        padding: const EdgeInsets.symmetric(horizontal: 2.5),
        textStyle: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        backgroundColor: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
          side: const BorderSide(
            color: Colors.green,
            width: 2.2,
          ),
        ),
      ),
      child: Text(
        text,
      ),
    );
  }
}
