import 'package:flutter/material.dart';
import 'package:sqflite_midterm_project/pages/about_us.dart';
import 'package:sqflite_midterm_project/pages/diary_list.dart';

void main() {
  runApp(const MyApp());
  WidgetsFlutterBinding.ensureInitialized();
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(
          seedColor: Colors.greenAccent,
          brightness: Brightness.dark,
        ),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePage();
}

class _MyHomePage extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
        centerTitle: true,
      ),
      drawer: Drawer(
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            const DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.green,
              ),
              child: Text('Drawer Header'),
            ),
            drawerListTile(context, 'Home', const MyApp()),
            const Divider(color: Colors.white),
            drawerListTile(context, 'Diary List', const DiaryList()),
            const Divider(color: Colors.white),
            drawerListTile(context, 'About Us', const AboutUs()),
          ],
        ),
      ),
      body: const Padding(
        padding: EdgeInsets.all(8),
        child: Center(
          child: Text('Welcome to home page!'),
        ),
      ),
    );
  }

  ListTile drawerListTile(BuildContext context, String text, Widget page) {
    return ListTile(
      title: Text(text),
      onTap: () {
        Navigator.pop(context); // Close the drawer
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => page));
      },
    );
  }
}