import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;

import '../data_model/product_data_model.dart';


class EditProduct extends StatefulWidget {
  const EditProduct({super.key, required this.product});

  final ProductDataModel product;

  @override
  State<EditProduct> createState() => _EditProductState();
}

class _EditProductState extends State<EditProduct> {
  TextEditingController nameController = TextEditingController();
  TextEditingController priceController = TextEditingController();
  TextEditingController descController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  // Http post request
  Future editProduct() async {
    return await http.post(
      Uri.parse("http://10.0.2.2/android/update_product.php"),
      body: {
        "pid": widget.product.pid.toString(),
        "name": nameController.text,
        "price": priceController.text,
        "description": descController
      },
    );
  }

  void _onConfirm(context) async {
    await editProduct();
    // Remove all existing routes until the Home.dart, then rebuild Home.
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
  }

  @override
  void initState() {
    nameController = TextEditingController(text: widget.product.name);
    priceController = TextEditingController(text: widget.product.price);
    descController = TextEditingController(text: widget.product.description);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Edit product"),
        centerTitle: true,
      ),
      bottomNavigationBar: BottomAppBar(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            ElevatedButton.icon(
              label: const Text('Save'),
              icon: const Icon(Icons.save),
              onPressed: () {
                // Validate returns true if the form is valid, or false otherwise.
                if (_formKey.currentState!.validate()) {
                  // If the form is valid, display a snack bar. In the real world,
                  // you'd often call a server or save the information in a database.
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(content: Text('Edit product successful...')),
                  );
                }
                _onConfirm(context);
              },
            ),
            ElevatedButton.icon(
              label: const Text('Cancel'),
              icon: const Icon(Icons.cancel_rounded),
              onPressed: () => Navigator.of(context)
                  .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false),
            )
          ],
        ),
      ),
      body: Container(
        height: double.infinity,
        padding: const EdgeInsets.all(20),
        child: Column(
          children: <Widget>[
            buildTextFormField(ctrl: nameController, label: 'Name', errorText: 'Enter product name'),
            buildTextFormField(ctrl: priceController, label: 'Price', errorText: 'Enter product price'),
            buildTextFormField(ctrl: descController, label: 'Description', errorText: 'Enter product description'),
          ],
        ),
      ),
    );
  }

  TextFormField buildTextFormField({required String label, required final ctrl, required String errorText}) {
    return TextFormField(
      controller: ctrl,
      decoration: InputDecoration(
        labelText: '$label:',
      ),
      // The validator receives the text that the user has entered.
      validator: (value) {
        if (value == null || value.isEmpty) {
          return errorText;
        }
        return null;
      },
    );
  }
}
