import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'DataModel/weatherdata.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  final _cityTextController = TextEditingController();
  WeatherData? _weather;

  // Http get request to get weather data
  Future<void> getWeather(String city) async {
    String url = "https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=80d2a0d7c643385e620b1b8400c46464&lang=th&units=metric";
    final response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {// If the server did return a 200 OK response,
      // then parse the JSON.
      print(response.body);
      setState(() {
        _weather = weatherDataFromJson(response.body);
      });
      //return weatherDataFromJson(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      setState(() {
        _weather = null;
      });
      throw Exception('Failed to get weather data');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            if(_weather == null)
              Column(
                children: [
                  Text(
                    "0 °C",
                    style: TextStyle(fontSize: 40),
                  ),
                  Text("No city")
                ],
              ),
            if(_weather != null)
              Column(
                children: [
                  Image.network(
                      'http://openweathermap.org/img/w/${_weather?.weather[0].icon}.png'
                  ),
                  Text(
                    "${_weather?.main.temp}°C",
                    style: TextStyle(fontSize: 40),
                  ),
                  Text("${_weather?.name}")
                ],
              ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 50),
              child: SizedBox(
                width: 150,
                child: TextField(
                    controller: _cityTextController,
                    decoration: InputDecoration(labelText: 'City'),
                    textAlign: TextAlign.center),
              ),
            ),
            ElevatedButton(onPressed: _search, child: Text('Search'))
          ],
        ),
      ),
    );
  }

  void _search() async {
    getWeather(_cityTextController.text);
  }
}