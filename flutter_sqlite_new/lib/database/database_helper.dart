import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

import '../model/profile_model.dart';


class DatabaseHelper {
  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  static Database? _database;
  Future<Database> get database async => _database ??= await _initDatabase();

  Future<Database> _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, 'db_profile.db');
    return await openDatabase(
      path,
      version: 1,
      onCreate: _onCreate,
    );
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
      CREATE TABLE profileDB (
          id INTEGER PRIMARY KEY,
          firstname TEXT,
          lastname TEXT,
          email TEXT,
          phone TEXT,
          image TEXT
      )
      ''');
  }

  Future<List<ProfileModel>> getProfiles() async {
    Database db = await instance.database;
    var profiles = await db.query('profileDB', orderBy: 'firstname');
    List<ProfileModel> groceryList = profiles.isNotEmpty
        ? profiles.map((c) => ProfileModel.fromMap(c)).toList()
        : [];
    return groceryList;
  }

  Future<List<ProfileModel>> getProfile(int id) async{
    Database db = await instance.database;
    var profile = await db.query('profileDB', where: 'id = ?', whereArgs: [id]);
    List<ProfileModel> profileList = profile.isNotEmpty
        ? profile.map((c) => ProfileModel.fromMap(c)).toList()
        : [];
    return profileList;
  }

  Future<int> add(ProfileModel profile) async {
    Database db = await instance.database;
    return await db.insert('profileDB', profile.toMap());
  }

  Future<int> remove(int id) async {
    Database db = await instance.database;
    return await db.delete('profileDB', where: 'id = ?', whereArgs: [id]);
  }

  Future<int> update(ProfileModel profile) async {
    Database db = await instance.database;
    return await db.update('profileDB', profile.toMap(),
        where: "id = ?", whereArgs: [profile.id]);
  }
}