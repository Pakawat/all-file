import 'dart:convert';
import 'package:flutter/material.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart' as p;
import 'package:uuid/uuid.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      // Hide the debug banner
      debugShowCheckedModeBanner: false,
      title: 'Upload Image to MySQL Server',
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  File? _image;
  XFile? pickedImage;
  final ImagePicker _picker = ImagePicker();

  List<String> ctrlList = [];
  var nameController = TextEditingController();
  var priceController = TextEditingController();
  var descController = TextEditingController();

  final _nameFormKey = GlobalKey<FormState>();
  final _priceFormKey = GlobalKey<FormState>();
  final _descFormKey = GlobalKey<FormState>();

  Future<void> uploadImage(filepath, List<String> ctrlList) async {
    String uploadUrl = 'http://10.0.2.2/upload_image/upload_image_multipart.php';
    var request = http.MultipartRequest('POST', Uri.parse(uploadUrl));
    request.files.add(
        await http.MultipartFile.fromPath('uploadedfile', filepath)
    );
    request.fields['name'] = ctrlList[0];
    request.fields['price'] = ctrlList[1];
    request.fields['description'] = ctrlList[2];
    var response = await request.send();
    if (response.statusCode == 200) {
      debugPrint("Upload successful-MultipartFile");
    } else {
      debugPrint("Error during connection to server-MultipartFile");
    }
  }

  Future<void> uploadImageBase64(imageFile, List<String> ctrlList) async {
    String uploadUrl = "http://10.0.2.2/upload_image/upload_image_base64.php";
    //convert file image to Base64 encoding
    String extension = p.extension(imageFile.path);
    String fileName = const Uuid().v4() + extension;
    String baseImage = base64Encode(imageFile.readAsBytesSync());

    var response = await http.post(Uri.parse(uploadUrl), body: {
      'image': baseImage,
      'filename': fileName,
      'name': ctrlList[0],
      'price': ctrlList[1],
      'description': ctrlList[2]
    });
    if (response.statusCode == 200) {
      debugPrint("Upload successful-Base64");
    } else {
      debugPrint("Error during connection to server-Base64");
    }
  }

  // Implementing the image picker
  Future<void> _openImagePicker() async {
    pickedImage = await _picker.pickImage(source: ImageSource.gallery);
    if (pickedImage != null) {
      setState(() {
        _image = File(pickedImage!.path);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Upload Image File'),
          backgroundColor: Colors.grey,
        ),
        body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Column(children: [
              Center(
                child: ElevatedButton(
                  child: const Text('Select An Image'),
                  onPressed: () {
                    _openImagePicker();
                  },
                  style: ElevatedButton.styleFrom(
                    primary: Colors.grey, // Set the button color to grey
                  ),
                ),
              ),
              const SizedBox(height: 10),
              Container(
                alignment: Alignment.center,
                width: double.infinity,
                height: 300,
                color: Colors.grey[300],
                child: _image != null
                    ? Image.file(_image!, fit: BoxFit.cover)
                    : const Text('Please select an image'),
              ),
              SizedBox(height: 10),
              Container(
                padding: const EdgeInsets.all(8),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(7.0),
                    color: Colors.grey[300]
                ),
                child: Column(
                  children: [
                    const Text('Add product detail', style: TextStyle(fontSize: 25, color: Colors.black)),
                    textForm(ctrl: nameController, formKey: _nameFormKey, labelText: 'Product name', checkNullText: 'Please enter product name'),
                    textForm(ctrl: priceController, formKey: _priceFormKey, labelText: 'Product price', checkNullText: 'Please enter product price'),
                    textForm(ctrl: descController, formKey: _descFormKey, labelText: 'Product description', nullMaxLine: true, checkNullText: 'Please enter product description'),
                  ],
                ),
              ),
              const SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        padding: const EdgeInsets.all(10.0),
                        minimumSize: const Size(120, 25),
                        backgroundColor: Colors.grey
                    ),
                    child: const Column(
                      children: [
                        Text('Save', style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold)),
                        Text('(Base64)', style: TextStyle(fontSize: 14, fontWeight: FontWeight.normal))
                      ],
                    ),
                    onPressed: () {
                      //add value in text controller to list
                      ctrlList.add(nameController.text);
                      ctrlList.add(priceController.text);
                      ctrlList.add(descController.text);
                      debugPrint(
                          '''Debug print controller list\n${ctrlList[0]}\n${ctrlList[1]}\n${ctrlList[2]}'''
                      );
                      //start uploading image
                      uploadImageBase64(_image, ctrlList);
                      // Validate returns true if the form is valid, or false otherwise.
                      if (_nameFormKey.currentState!.validate() && _priceFormKey.currentState!.validate() && _descFormKey.currentState!.validate()) {
                        // If the form is valid, display a snack bar. In the real world,
                        // you'd often call a server or save the information in a database.
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(content: Text('Upload successfully, by Base64')),
                        );
                      }
                    },
                  ),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        padding: const EdgeInsets.all(10.0),
                        minimumSize: const Size(120, 25),
                        backgroundColor: Colors.grey
                    ),
                    child: const Column(
                      children: [
                        Text('Save', style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold)),
                        Text('(Multipart)', style: TextStyle(fontSize: 14, fontWeight: FontWeight.normal))
                      ],
                    ),
                    onPressed: () {
                      //add value in text controller to list
                      ctrlList.add(nameController.text);
                      ctrlList.add(priceController.text);
                      ctrlList.add(descController.text);
                      debugPrint(
                          '''Debug print controller list\n${ctrlList[0]}\n${ctrlList[1]}\n${ctrlList[2]}'''
                      );
                      //start uploading image
                      uploadImage(pickedImage!.path, ctrlList);
                      // Validate returns true if the form is valid, or false otherwise.
                      if (_nameFormKey.currentState!.validate() && _priceFormKey.currentState!.validate() && _descFormKey.currentState!.validate()) {
                        // If the form is valid, display a snack bar. In the real world,
                        // you'd often call a server or save the information in a database.
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(content: Text('Upload successfully, by Multipart')),
                        );
                      }
                    },
                  ),
                ],
              ),
            ]),
          ),
        )));
  }
}


Form textForm({
  required final ctrl,
  required final formKey,
  String? labelText,
  bool? nullMaxLine,
  String? checkNullText})
{
  return Form(
    key: formKey,
    child: Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
      child: TextFormField(
        controller: ctrl,
        // The validator receives the text that the user has entered.
        validator: (value) {
          if (value == null || value.isEmpty) {
            return checkNullText;
          }
          return null;
        },
        maxLines: nullMaxLine == true ? null : 1,
        decoration: InputDecoration(
            border: const UnderlineInputBorder(),
            labelText: '$labelText: ',
            filled: true,
            fillColor: Colors.white
        ),
      ),
    ),
  );
}
