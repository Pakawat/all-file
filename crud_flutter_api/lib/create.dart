import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class Create extends StatefulWidget {
  @override
  _CreateState createState() => _CreateState();
}

class _CreateState extends State<Create> {
  // Handles text
  TextEditingController nameController = TextEditingController();
  TextEditingController ageController = TextEditingController();

  // Http post request to create new data
  Future _createStudent() async {
    return await http.post(
      Uri.parse("http://10.0.2.2/flutter_api/create.php"),
      body: {
        "name": nameController.text,
        "age": ageController.text,
      },
    );
  }

  void _onConfirm(context) async {
    await _createStudent();

    // Remove all existing routes until the Home.dart, then rebuild Home.
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Create"),
      ),
      bottomNavigationBar: BottomAppBar(
        child: ElevatedButton.icon(
          label: Text('Save'),
          icon: Icon(Icons.save),
          style: ElevatedButton.styleFrom(
            primary: Colors.blue,
            onPrimary: Colors.white,
          ),
          onPressed: () {
            _onConfirm(context);
          },
        ),
      ),
      body: Container(
        height: double.infinity,
        padding: EdgeInsets.all(20),
        child: Column(
          children: <Widget>[
            Container(
                child: TextField(
                  controller: nameController,
                  decoration: InputDecoration(
                    labelText: "Student Name:",
                    hintText: "Enter student name",
                  ),
                )),
            Container(
                child: TextField(
                  controller: ageController,
                  decoration: InputDecoration(
                    labelText: "Age:",
                    hintText: "Enter student age",
                  ),
                )),
          ],
        ),
      ),
    );
  }
}
