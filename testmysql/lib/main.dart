import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'insert.dart';
import 'product.dart';



void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Home(),
    );
  }
}

class Home extends StatefulWidget {
  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<Home> {
  late Future<List<Product>> products;

  @override
  void initState() {
    super.initState();
    products = getProductList();
  }

  Future<List<Product>> getProductList() async {
    String url = "http://10.0.2.2/android/list_products.php";
    final response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return productFromJson(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load products');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('CRUD MySQL'),
      ),
      body: Center(
        child: FutureBuilder<List<Product>>(
          future: products,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            // By default, show a loading spinner.
            if (!snapshot.hasData) return CircularProgressIndicator();
            // Render student lists
            return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                var data = snapshot.data[index];
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ListTile(
                    title: Text(
                      data.name,
                      style: TextStyle(fontSize: 20),
                    ),
                    subtitle: Text(data.description), // เพิ่มรายละเอียดเป็น subtitle
                    trailing: Text(
                      '${data.price}', // แสดงราคาใน trailing
                      style: TextStyle(fontSize: 16,),
                    ),

                    //onTap: () {
                      //   Navigator.push(
                      //   context,
                      //   MaterialPageRoute(
                      //   builder: (context) => Details(student: data)),
                      //   );
                      // },

                  ),
                );
              },
            );

          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => Create()),
          );
        },
      ),
    );
  }
}
